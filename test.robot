*** Settings ***
Resource          requriements.resource

*** Test Cases ***
Verify_system_bay_id-1
    log    /api/system/kpi?bay_id=1
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/system/kpi?bay_id=1
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_robots_bay_id-1
    log    /api/robots?bay_id=1
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/robots?bay_id=1
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/AAT01
    log    /api/shelf/AAT01
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/AAT01
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/AAT02
    log    /api/shelf/AAT02
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/AAT02
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/AAT04
    log    /api/shelf/AAT04
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/AAT04
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/AAT05
    log    /api/shelf/AAT05
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/AAT05
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_robots/AWP01
    log    /api/robots/AWP01
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/robots/AWP01
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_robots/AWP02
    log    /api/robots/AWP02
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/robots/AWP02
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVTN04
    log    /api/devices/AVTN04
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVTN04
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVTN05
    log    /api/devices/AVTN05
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVTN05
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVTN06
    log    /api/devices/AVTN06
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVTN06
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVTN07
    log    /api/devices/AVTN07
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVTN07
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVAL07
    log    /api/devices/AVAL07
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVAL07
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVAL08
    log    /api/devices/AVAL08
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVAL08
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVAL09
    log    /api/devices/AVAL09
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVAL09
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVAL10
    log    /api/devices/AVAL10
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVAL10
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVAL15
    log    /api/devices/AVAL15
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVAL15
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACWP05
    log    /api/devices/ACWP05
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACWP05
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AEOE18
    log    /api/devices/AEOE18
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AEOE18
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACNP16
    log    /api/devices/ACNP16
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACNP16
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACNP17
    log    /api/devices/ACNP17
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACNP17
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACNP18
    log    /api/devices/ACNP18
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACNP18
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVTN11
    log    /api/devices/AVTN11
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVTN11
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/AAT11
    log    /api/shelf/AAT11
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/AAT11
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/AAT12
    log    /api/shelf/AAT12
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/AAT12
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/AAT17
    log    /api/shelf/AAT17
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/AAT17
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/AAT18
    log    /api/shelf/AAT18
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/AAT18
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_robots/AWP07
    log    /api/robots/AWP07
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/robots/AWP07
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_robots/AWP08
    log    /api/robots/AWP08
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/robots/AWP08
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACBP04
    log    /api/devices/ACBP04
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACBP04
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACNP11
    log    /api/devices/ACNP11
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACNP11
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACHD07
    log    /api/devices/ACHD07
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACHD07
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACHD08
    log    /api/devices/ACHD08
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACHD08
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACHD11
    log    /api/devices/ACHD11
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACHD11
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACNP12
    log    /api/devices/ACNP12
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACNP12
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACNP13
    log    /api/devices/ACNP13
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACNP13
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACNP14
    log    /api/devices/ACNP14
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACNP14
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACNP15
    log    /api/devices/ACNP15
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACNP15
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACHD19
    log    /api/devices/ACHD19
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACHD19
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACHD10
    log    /api/devices/ACHD10
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACHD10
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACHD12
    log    /api/devices/ACHD12
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACHD12
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACNP09
    log    /api/devices/ACNP09
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACNP09
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACNP10
    log    /api/devices/ACNP10
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACNP10
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACNP19
    log    /api/devices/ACNP19
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACNP19
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACTE03
    log    /api/devices/ACTE03
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACTE03
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACTE04
    log    /api/devices/ACTE04
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACTE04
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACTE05
    log    /api/devices/ACTE05
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACTE05
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACWS03
    log    /api/devices/ACWS03
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACWS03
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/ABT34
    log    /api/shelf/ABT34
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/ABT34
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/ABT35
    log    /api/shelf/ABT35
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/ABT35
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/ABT36
    log    /api/shelf/ABT36
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/ABT36
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/ABT37
    log    /api/shelf/ABT37
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/ABT37
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_robots/AWP05
    log    /api/robots/AWP05
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/robots/AWP05
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_robots/AWP06
    log    /api/robots/AWP06
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/robots/AWP06
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVAL01
    log    /api/devices/AVAL01
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVAL01
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVAL02
    log    /api/devices/AVAL02
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVAL02
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVTN01
    log    /api/devices/AVTN01
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVTN01
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVTN02
    log    /api/devices/AVTN02
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVTN02
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACHD01
    log    /api/devices/ACHD01
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACHD01
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACHD02
    log    /api/devices/ACHD02
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACHD02
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACHD03
    log    /api/devices/ACHD03
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACHD03
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACWP01
    log    /api/devices/ACWP01
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACWP01
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACWP02
    log    /api/devices/ACWP02
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACWP02
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVC001
    log    /api/devices/AVC001
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVC001
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACWP03
    log    /api/devices/ACWP03
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACWP03
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACWP04
    log    /api/devices/ACWP04
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACWP04
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AIRT12
    log    /api/devices/AIRT12
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AIRT12
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ACWS02
    log    /api/devices/ACWS02
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ACWS02
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVTN03
    log    /api/devices/AVTN03
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVTN03
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVAL03
    log    /api/devices/AVAL03
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVAL03
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVAL04
    log    /api/devices/AVAL04
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVAL04
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/AVAL05
    log    /api/devices/AVAL05
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/AVAL05
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/AAP08
    log    /api/shelf/AAP08
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/AAP08
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/AAP09
    log    /api/shelf/AAP09
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/AAP09
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_robots/AWOP09
    log    /api/robots/AWOP09
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/robots/AWOP09
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ALII15
    log    /api/devices/ALII15
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ALII15
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ALII16
    log    /api/devices/ALII16
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ALII16
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ALDI11
    log    /api/devices/ALDI11
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ALDI11
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/ALDI12
    log    /api/devices/ALDI12
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/ALDI12
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/BBE01
    log    /api/shelf/BBE01
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/BBE01
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/BBE02
    log    /api/shelf/BBE02
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/BBE02
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/BBE03
    log    /api/shelf/BBE03
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/BBE03
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_shelf/BBE04
    log    /api/shelf/BBE04
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/shelf/BBE04
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_robots/BWOP03
    log    /api/robots/BWOP03
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/robots/BWOP03
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_robots/BWOP04
    log    /api/robots/BWOP04
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/robots/BWOP04
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEOE19
    log    /api/devices/BEOE19
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEOE19
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEOE20
    log    /api/devices/BEOE20
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEOE20
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEOE21
    log    /api/devices/BEOE21
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEOE21
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEOE23
    log    /api/devices/BEOE23
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEOE23
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEOE24
    log    /api/devices/BEOE24
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEOE24
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEOE25
    log    /api/devices/BEOE25
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEOE25
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEOE26
    log    /api/devices/BEOE26
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEOE26
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEOE27
    log    /api/devices/BEOE27
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEOE27
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEOE28
    log    /api/devices/BEOE28
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEOE28
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BIRT16
    log    /api/devices/BIRT16
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BIRT16
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEME11
    log    /api/devices/BEME11
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEME11
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEME12
    log    /api/devices/BEME12
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEME12
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEME13
    log    /api/devices/BEME13
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEME13
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEME15
    log    /api/devices/BEME15
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEME15
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BCWP06
    log    /api/devices/BCWP06
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BCWP06
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BIRT17
    log    /api/devices/BIRT17
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BIRT17
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEPE10
    log    /api/devices/BEPE10
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEPE10
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEPE11
    log    /api/devices/BEPE11
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEPE11
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BEPEOE22
    log    /api/devices/BEPEOE22
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BEPEOE22
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

Verify_devices/BVAL14
    log    /api/devices/BVAL14
    log    ${host}
    ${val2}    Catenate    SEPARATOR=    ${host}    /api/devices/BVAL14
    Create Session    api    ${host}
    ${response}    GET On Session    api    ${val2}
    should contain    ${response.json()['msg']}    ok

test
    log    1
